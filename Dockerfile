FROM ubuntu:14.04
MAINTAINER Kevin Weirauch

RUN apt-get -y update && apt-get -y install wget

#Download Slic3r Version 1.1.7
RUN wget http://dl.slic3r.org/linux/slic3r-linux-x86_64-1-1-7-stable.tar.gz

#Extract Slic3r
#Executable exists in /Slic3r/bin/
RUN tar -xvf slic3r-linux-x86_64-1-1-7-stable.tar.gz

#Define working directory.
WORKDIR /Slic3r/bin

